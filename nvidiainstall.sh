#!/bin/bash
echo
 echo "************************************************************************"
 echo "**                                                                    **"
 echo "**                           NVIDIAINSTALL                            **"  
 echo "**                                                                    **"
 echo "************************************************************************"
echo
echo "Este es el script de Roberto Díaz, para instalar el driver privativo de nvidia en tu distribución" 
#leer readme de gitlab para obtener más información sobre la compatibilidad del script
echo
echo "Y es su 3º versión"
echo
sleep 3s
echo "Vamos a ello......"
echo ""
sleep 2s
echo  "Comprobaré que distribución usas e instalaré el driver de nvidia:"

    dist=$(tr -s ' \011' '\012' < /etc/issue | head -n 1)
    case $dist in
        Debian)     echo "OK, tu distribución es Debian" ; sudo apt install -y nvidia-detect ; nvidia-detect ; driver=$(nvidia-detect | grep "nvidia-" | sed 's/ //g' ) ; sudo apt install -y "$driver" ; echo ""
   		    sleep 3s ;;

        Ubuntu)     echo "OK, tu distribución es Ubuntu" ; sudo ubuntu-drivers autoinstall
            sleep 3s ;;

        Linux)      echo "OK, tu distribución es Linux Mint" ; sudo ubuntu-drivers autoinstall
   		    sleep 3s ;;

        KDE)        echo "OK, tu distribución es KDE Neon" ; sudo ubuntu-drivers autoinstall
   		    sleep 3s ;;

        Manjaro)    echo "OK, tu distribución es Manjaro" ; sudo mhwd -a pci nonfree 0300
   		    sleep 3s ;;

        Fedora)     echo "OK, tu distribución es Fedora" ; sudo dnf install -y akmod-nvidia ; sudo dnf install -y xorg-x11-drv-nvidia-cuda ; sudo dnf update -y
   		    sleep 3s ;;

        Arch)       echo "OK, tu distribución es Arch linux o algún derivado" ; sudo pacman -Syu nvidia-dkms nvidia-settings --noconfirm 
   		    sleep 3s ;;

        Pop!_OS)       echo "OK, tu distribución es Pop!_OS" ; sudo apt install -y system76-driver-nvidia
   		    sleep 3s ;;
        
        Zorin)       echo "OK, tu distribución es Zorin Os" ; sudo ubuntu-drivers autoinstall
   		    sleep 3s ;;

        * )         echo "Tu Distribución no es compatible.....operación abortada.....saliendo....." ; exit;;
            
    esac

echo
echo "Este script cumple las cuatro libertades del software libre, que son las siguientes:"
echo ""
echo "La libertad de ejecutar el software como te plazca y con cualquier objetivo.\nLa libertad de estudiar como funciona el programa y cambiarlo a tu gusto.\nLa libertad de poder redistribuir copias del programa a los demás.\nLa libertad de poder distribuir también tus mejoras al programa original."
sleep 3s
echo
echo
echo "Gracias por usar mi script, puedes contactarme a través una cuenta de correo electrónico que dejaré a continuación por si necesitas algo querid@ usuari@. Acepto sugerencias, modificaciones del script... Todas las ideas serán bienvenidas ;) "
echo
echo "scriptinglinuxbyrober@gmail.com"
echo
echo
echo "Ahora deberías reiniciar el sistema para que todos los modulos de nvidia se carguen en el kernel."
echo
sleep 1s

    read -p "¿Quieres reiniciar ahora (s/n)?" sn
    case $sn in
        [Ss]* )  sudo reboot;;
        [Nn]* ) exit;;
        * ) echo "Por favor, pulsa s o n.";;
    esac
done
